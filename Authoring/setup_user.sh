#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
trap 'exit 128' INT

if [ -z "${2}" ] ; then
    echo " Please provide a e-mail address and a user'a real name (quoted)."

    exit 1
fi

set -x

user_email="${1}"
user_name="${2}"

git config --local user.email "${user_email}"
git config --local user.signingkey "${user_email}"
git config --local user.name "${user_name}"
