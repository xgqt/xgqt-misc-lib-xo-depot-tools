#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# Copyright (c) 2024-2025, Maciej Barć <xgqt@xgqt.org>

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r source_root="$(realpath "${script_root}/../")"
cd "${source_root}"

cd "${source_root}/xo-depot-tools/bin"

shopt -s extglob
shopt -s nullglob
shopt -u dotglob

printf "%s " *
printf "\n"
