#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
set -x
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/../")"
cd "${source_root}"

jq_message='[{"msg": "ok"}]'
jq_query='.[0].msg'
jq_expected='"ok"'

bash ./xo-depot-tools/src/main/sh/download-jq.bash

js_result=$(echo "${jq_message}" \
                | bash ./xo-depot-tools/src/main/sh/wrap-jq.bash "${jq_query}")

if [ "${js_result}" = "${jq_expected}" ] ; then
    echo "Testing with JQ passed."

    exit 0
else
    echo "Testing with JQ failed."

    exit 1
fi
