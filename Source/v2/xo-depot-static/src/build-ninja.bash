#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"
declare -r source_root="$(realpath "${script_root}/../")"

declare -r files_root="${script_root}/files"
declare -r licenses_root="${files_root}/licenses"

declare -r bin_root="${source_root}/bin"
declare -r dist_root="${source_root}/dist"
declare -r tmp_root="${source_root}/.cache/tmp"

declare DSV_NINJA
: "${DSV_NINJA:=1.12.1}"

declare -r pn="ninja"
declare -r pv="${DSV_NINJA}"
declare -r p="${pn}-${pv}"

set -x

mkdir -p "${bin_root}"
mkdir -p "${dist_root}"
mkdir -p "${tmp_root}"

declare -r os="linux-$(uname -m)"
declare -r src_uri="https://github.com/ninja-build/${pn}/archive/refs/tags/v${pv}.tar.gz"

cd "${tmp_root}"
curl --location --silent --output "${p}.tar.gz" "${src_uri}"

tar xf "${p}.tar.gz"
cd "${p}"

sed -e "s|ldflags = \[|\0 '-static', |g" -i ./configure.py

python3 ./configure.py --bootstrap
strip "${pn}"
mv "${pn}" "${bin_root}/${p}-${os}"

cd "${tmp_root}"
mkdir -p "${p}-${os}"

cd "${p}-${os}"

mkdir -p licenses
cp "${tmp_root}/${p}/COPYING" "licenses/License-${pn}.txt"
cp "${licenses_root}/License-musl.txt" licenses
tar --create --auto-compress --file licenses.tar.xz licenses
rm -f -r licenses

cp "${bin_root}/${p}-${os}" .

cd "${tmp_root}"
zip -r "${dist_root}/${p}-${os}.zip" "${p}-${os}"

cd "${script_root}"
rm -f -r "${tmp_root:?.}/${pn}"*
