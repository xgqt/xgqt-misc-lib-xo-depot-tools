#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"
declare -r source_root="$(realpath "${script_root}/../")"

declare -r files_root="${script_root}/files"
declare -r licenses_root="${files_root}/licenses"

declare -r bin_root="${source_root}/bin"
declare -r dist_root="${source_root}/dist"
declare -r tmp_root="${source_root}/.cache/tmp"

declare DSV_LIBARCHIVE
: "${DSV_LIBARCHIVE:=3.7.4}"

declare -r pn="libarchive"
declare -r pv="${DSV_LIBARCHIVE}"
declare -r p="${pn}-${pv}"

set -x

mkdir -p "${bin_root}"
mkdir -p "${dist_root}"
mkdir -p "${tmp_root}"

declare -r os="linux-$(uname -m)"
declare -r src_uri="https://github.com/libarchive/${pn}/archive/refs/tags/v${pv}.tar.gz"

cd "${tmp_root}"
curl --location --silent --output "${p}.tar.gz" "${src_uri}"

tar xf "${p}.tar.gz"
cd "${p}"

autoreconf --force --install

declare bsdtool=""
declare -a bsdtools=(
    bsdcat
    bsdcpio
    bsdtar
    bsdunzip
)

declare -a myconf=(
    # Libraries.
    --enable-shared="no"
    --enable-static="no"

    # ON
    --enable-acl
    --enable-xattr
    --with-bz2lib
    --with-iconv
    --with-lzma
    --with-openssl
    --with-xml2
    --with-zlib
    --with-zstd

    # OFF
    --without-expat
    --without-libb2
    --without-lz4
    --without-lzo2
    --without-nettle
    --without-cng
)

# Binaries.
bsdtool=""
for bsdtool in "${bsdtools[@]}" ; do
    myconf+=(
        "--enable-${bsdtool}"
        "--enable-${bsdtool}=static"
    )
done

./configure "${myconf[@]}"

make -j"$(nproc)" CFLAGS="--static" LDFLAGS="-static"

bsdtool=""
for bsdtool in "${bsdtools[@]}" ; do
    strip "${bsdtool}"
    mv "${bsdtool}" "${bin_root}/${bsdtool}-${p}-${os}"
done

cd "${tmp_root}"
mkdir -p "${p}-${os}"

cd "${p}-${os}"

mkdir -p licenses
cp "${tmp_root}/${p}/COPYING" "licenses/License-${pn}.txt"
cp "${licenses_root}/License-acl.txt" licenses
cp "${licenses_root}/License-attr.txt" licenses
cp "${licenses_root}/License-bzip2.txt" licenses
cp "${licenses_root}/License-libxml2.txt" licenses
cp "${licenses_root}/License-musl.txt" licenses
cp "${licenses_root}/License-openssl.txt" licenses
cp "${licenses_root}/License-xz.txt" licenses
cp "${licenses_root}/License-zlib.txt" licenses
cp "${licenses_root}/License-zstd.txt" licenses
tar --create --auto-compress --file licenses.tar.xz licenses
rm -f -r licenses

bsdtool=""
for bsdtool in "${bsdtools[@]}" ; do
    cp "${bin_root}/${bsdtool}-${p}-${os}" .
    mv "${bsdtool}-${p}-${os}" "${bsdtool}"
done

cd "${tmp_root}"
zip -r "${dist_root}/${p}-${os}.zip" "${p}-${os}"

cd "${script_root}"
rm -f -r "${tmp_root:?.}/${pn}"*
