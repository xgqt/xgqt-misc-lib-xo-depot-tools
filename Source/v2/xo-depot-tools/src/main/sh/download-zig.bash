#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r source_root="$(realpath "${script_root}/../../../")"
declare -r bin_root="${source_root}/bin"
declare -r lib_root="${source_root}/lib"
declare -r tmp_root="${source_root}/.cache/tmp"

declare DTV_ZIG
: "${DTV_ZIG:=0.13.0}"

declare -r pn="zig"
declare -r pv="${DTV_ZIG}"
declare -r exe_path="${bin_root}/${pn}"

if [[ -e "${exe_path}" ]] ; then
    echo " [DEBUG   ] ${pn} is already installed as ${exe_path}"

    exit 0
fi

echo " [DEBUG   ] Downloading ${pn}"
set -x

mkdir -p "${bin_root}"
mkdir -p "${tmp_root}"
mkdir -p "${lib_root}"

declare -r os_arch="$(uname -m)"
declare p=""

case "${os_arch}" in
    x86_64 )
        p="${pn}-linux-x86_64-${pv}"
        ;;
    * )
        echo " [ERROR   ] Unsupported architecture"

        exit 1
        ;;
esac

declare -r src_uri="https://ziglang.org/download/${pv}/${p}.tar.xz"

cd "${tmp_root}"
curl --location --silent --output "${pn}.tar.gz" "${src_uri}"

tar xf "${pn}.tar.gz"
mv "./${p}" "${lib_root}/${pn}"

ln -f -s "../lib/${pn}/${pn}" "${exe_path}"
rm -r "${tmp_root}"
