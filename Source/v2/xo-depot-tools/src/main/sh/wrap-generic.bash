#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap "exit 128" INT

declare -r script_path="${0}"
declare -r script_name="$(basename "${script_path}")"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r depot_root="$(realpath "${script_root}/../../../")"
declare -r bin_root="${depot_root}/bin"

PATH="${bin_root}:${PATH}"
export PATH

declare -r _target_bin_name="${script_name//wrap-/}"
declare -r target_bin_name="${_target_bin_name//.bash/}"

if ! command -v "${target_bin_name}" >/dev/null 2>&1 ; then
    bash "${script_root}/download-${target_bin_name}.bash"
fi

exec "${target_bin_name}" "${@}"
