#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r source_root="$(realpath "${script_root}/../../../")"
declare -r bin_root="${source_root}/bin"
declare -r lib_root="${source_root}/lib"

declare -r pn="az"
declare -r exe_path="${bin_root}/${pn}"

if [[ -e "${exe_path}" ]] ; then
    echo " [DEBUG   ] ${pn} is already installed as ${exe_path}"

    exit 0
fi

echo " [DEBUG   ] Downloading ${pn}"
set -x

declare -r pve_root="${lib_root}/python-virtual-environment"
declare -r ENV_DIR="${pve_root}/${pn}" ; export ENV_DIR

mkdir -p "${bin_root}"
mkdir -p "${pve_root}"

python3 -m venv "${ENV_DIR}"
source "${ENV_DIR}/bin/activate"

declare -a -r python_packages=(
    azure-cli
    setuptools
)
declare -r -a pip_opts=(
    --quiet
    --require-virtualenv
)
python -m pip install "${pip_opts[@]}" "${python_packages[@]}"

ln -s "../lib/python-virtual-environment/${pn}/bin/${pn}" "${exe_path}"
