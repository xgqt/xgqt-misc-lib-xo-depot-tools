#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"
declare -r source_root="$(realpath "${script_root}/../../../")"

declare -r bin_root="${source_root}/bin"
declare -r lib_root="${source_root}/lib"
declare -r tmp_root="${source_root}/.cache/tmp"

declare DTV_LIBARCHIVE
: "${DTV_LIBARCHIVE:=3.7.4}"

declare -r pn="libarchive"
declare -r pv="${DTV_LIBARCHIVE}"
declare -r exe_path="${bin_root}/bsdtar"  # But this installs more executables!

if [[ -e "${exe_path}" ]] ; then
    echo " [DEBUG   ] ${pn} is already installed as ${exe_path}"

    exit 0
fi

echo " [DEBUG   ] Downloading ${pn}"
set -x

mkdir -p "${bin_root}"
mkdir -p "${tmp_root}"
mkdir -p "${lib_root}/static"

declare -r os_kernel="$(uname -s)"
declare -r os="${os_kernel,,}-$(uname -m)"

declare -r p="${pn}-${pv}-${os}"
declare -r src_uri="https://xgqt.org/distfiles/binmisc/${p}.zip"

cd "${tmp_root}"
curl --location --silent --output "${p}.zip" "${src_uri}"

unzip -o "${p}.zip"

declare bsdtool=""
declare -a bsdtools=(
    bsdcat
    bsdcpio
    bsdtar
    bsdunzip
)

for bsdtool in "${bsdtools[@]}" ; do
    mv "./${p}/${bsdtool}" "${lib_root}/static/"
    ln -f -s "../lib/static/${bsdtool}" "${bin_root}"
done

rm -f -r "${tmp_root}"
