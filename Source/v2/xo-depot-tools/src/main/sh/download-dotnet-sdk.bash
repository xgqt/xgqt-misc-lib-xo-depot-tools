#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r depot_root="$(realpath "${script_root}/../")"
declare -r tmp_root="${depot_root}/.cache/tmp"

declare -r bin_root="${depot_root}/bin"
declare -r lib_root="${depot_root}/lib"

declare DTV_DOTNET
: "${DTV_DOTNET:=9.0}"

declare -r pn="dotnet-sdk"
declare -r pv="${DTV_DOTNET}"
declare -r exe_path="${bin_root}/dotnet"

if [[ -e "${exe_path}" ]] ; then
    echo " [DEBUG   ] ${pn} is already installed as ${exe_path}"

    exit 0
fi

echo " [DEBUG   ] Downloading ${pn}"
set -x

mkdir -p "${bin_root}"
mkdir -p "${lib_root}"
mkdir -p "${tmp_root}"

cd "${tmp_root}"
curl --location --silent --output "dotnet-install.sh" "https://dot.net/v1/dotnet-install.sh"

declare -r dotnet_sdk_root="${lib_root}/${pn}"
mkdir -p "${dotnet_sdk_root}"

bash ./dotnet-install.sh --channel "${pv}" --install-dir "${dotnet_sdk_root}"
ln -s "../lib/${pn}/dotnet" "${exe_path}"

rm "${tmp_root}/dotnet-install.sh"
