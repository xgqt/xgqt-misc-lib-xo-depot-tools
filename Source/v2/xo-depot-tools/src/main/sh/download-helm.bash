#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r source_root="$(realpath "${script_root}/../../../")"
declare -r bin_root="${source_root}/bin"
declare -r lib_root="${source_root}/lib"
declare -r tmp_root="${source_root}/.cache/tmp"

declare DTV_HELM
: "${DTV_HELM:=3.15.4}"

declare -r pn="helm"
declare -r pv="${DTV_HELM}"
declare -r exe_path="${bin_root}/${pn}"

if [[ -e "${exe_path}" ]] ; then
    echo " [DEBUG   ] ${pn} is already installed as ${exe_path}"

    exit 0
fi

echo " [DEBUG   ] Downloading ${pn}"
set -x

mkdir -p "${bin_root}"
mkdir -p "${tmp_root}"
mkdir -p "${lib_root}/static"

declare -r os_arch="$(uname -m)"
declare p=""
declare s=""

case "${os_arch}" in
    x86_64 )
        p="${pn}-v${pv}-linux-amd64"
        s="linux-amd64"
        ;;
    * )
        echo " [ERROR   ] Unsupported architecture"

        exit 1
        ;;
esac

declare -r src_uri="https://get.helm.sh/${p}.tar.gz"

cd "${tmp_root}"
curl --location --silent --output "${pn}.tar.gz" "${src_uri}"

tar xf "${pn}.tar.gz"
mv "./${s}/${pn}" "${lib_root}/static"

ln -f -s "../lib/static/${pn}" "${exe_path}"
rm -r "${tmp_root}"
