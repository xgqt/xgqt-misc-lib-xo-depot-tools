#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare subscript_path=""
declare subscript_name=""

while read -r subscript_path ; do
    subscript_name="$(basename "${subscript_path}")"

    case "${subscript_name}" in
        download-azure-cli.bash )
            echo " [WARNING ] Skipping script: ${subscript_name}"

            continue
            ;;
    esac

    echo " [DEBUG   ] Executing script: ${subscript_name}"

    bash "${subscript_path}"
done < <(find "${script_root}/src/main/sh" -name "download-*.bash" -executable | sort)
