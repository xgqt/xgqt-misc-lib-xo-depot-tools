#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>

set -e
set -u
trap 'exit 128' INT

declare depot_tools_url="https://gitlab.com/xgqt/xgqt-misc-lib-xo-depot-tools"
: "${DEPOT_TOOLS_URL:=}"

if [[ -n "${DEPOT_TOOLS_URL}" ]] ; then
    depot_tools_url="${DEPOT_TOOLS_URL}"

    echo " [WRN] Discovered the DEPOT_TOOLS_URL variable in the process environment."
    echo " [DBG] Overwriting the depot_tools_url with value: ${depot_tools_url}"
fi

declare submodule_directory=""
: "${SUBMODULE_DIRECTORY:=}"

if [[ -n "${SUBMODULE_DIRECTORY}" ]] ; then
    submodule_directory="${SUBMODULE_DIRECTORY}"

    echo " [WRN] Discovered the SUBMODULE_DIRECTORY variable in the process environment."
    echo " [DBG] Overwriting the submodule_directory with value: ${submodule_directory}"
elif [[ -n "${1}" ]] ; then
    submodule_directory="${1}"

    echo " [DBG] Overwriting the submodule_directory with value: ${submodule_directory}"
else
    submodule_directory="$(pwd)/xo-depot-tools"

    echo " [DBG] Using the default submodule_directory value: ${submodule_directory}"
fi

declare depot_tools_version="1.2.0"
: "${DEPOT_TOOLS_VERSION:=}"

if [[ -n "${DEPOT_TOOLS_VERSION}" ]] ; then
    depot_tools_version="${DEPOT_TOOLS_VERSION}"

    echo " [WRN] Discovered the DEPOT_TOOLS_VERSION variable in the process environment."
    echo " [DBG] Overwriting the depot_tools_version with value: ${depot_tools_version}"
fi

set -x

mkdir -p "$(dirname "${submodule_directory}")"
git submodule add "${depot_tools_url}" "${submodule_directory}"

cd "${submodule_directory}"
git checkout --force "${depot_tools_version}"
