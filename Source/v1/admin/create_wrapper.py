#!/usr/bin/env python


# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


from argparse import ArgumentParser
from pathlib import Path


def parse_arguments():
    """Parse the passed CLI arguments."""

    parser = ArgumentParser(
        description="Create a wrapper for a tool of XO-Depot-Tools",
        epilog="Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>",
    )

    parser.add_argument(
        "-c",
        "--command",
        type=str,
        default="",
        required=True,
        help="command name to create a wrapper for",
    )
    parser.add_argument(
        "-d",
        "--downloader",
        type=str,
        default="",
        required=False,
        help="downloader name to use to retrieve a binary if it is not found",
    )
    parser.add_argument(
        "-w",
        "--wrapper",
        type=Path,
        default=Path("."),
        required=False,
        help="wrapper path to write the content to",
    )

    return parser.parse_args()


def main():
    """The main function."""

    parsed_arguments = parse_arguments()

    command_name = parsed_arguments.command

    if parsed_arguments.downloader == "":
        downloader_name = command_name
    else:
        downloader_name = parsed_arguments.downloader

    if str(parsed_arguments.wrapper) == ".":
        wrapper_path = Path(f"./xo-depot-tools/src/wrap-{command_name}.bash")
    else:
        wrapper_path = parsed_arguments.wrapper

    file_content = f"""#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap "exit 128" INT

declare -r script_path="${{0}}"
declare -r script_root="$(dirname "$(realpath "${{script_path}}")")"

declare -r depot_root="$(realpath "${{script_root}}/../")"
declare -r bin_root="${{depot_root}}/bin"

PATH="${{bin_root}}:${{PATH}}" ; export PATH

if ! command -v {command_name} >/dev/null 2>&1 ; then
    bash "${{script_root}}/download-{downloader_name}.bash"
fi

exec {command_name} "${{@}}"
"""

    with open(file=wrapper_path, encoding="UTF-8", mode="w") as opened_file:
        opened_file.write(file_content)

    print(f"The file {wrapper_path} has been written with success.")


if __name__ == "__main__":
    main()
