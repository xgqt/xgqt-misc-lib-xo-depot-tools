@echo off

:: This Source Code Form is subject to the terms of the Mozilla Public
:: License, v. 2.0. If a copy of the MPL was not distributed with this
:: file, You can obtain one at http://mozilla.org/MPL/2.0/.
:: Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>

winget install --scope machine --source winget ^
    7zip.7zip                                  ^
    BurntSushi.ripgrep.GNU                     ^
    Git.Git                                    ^
    GnuPG.GnuPG                                ^
    GnuPG.Gpg4win                              ^
    Gyan.FFmpeg                                ^
    SourceFoundry.HackFonts                    ^
    HandBrake.HandBrake                        ^
    Python.Launcher                            ^
    Neovim.Neovim                              ^
    OpenJS.NodeJS                              ^
    Microsoft.OpenJDK.21                       ^
    Microsoft.DotNet.SDK.8
