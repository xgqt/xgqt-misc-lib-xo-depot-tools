@echo off

:: This Source Code Form is subject to the terms of the Mozilla Public
:: License, v. 2.0. If a copy of the MPL was not distributed with this
:: file, You can obtain one at http://mozilla.org/MPL/2.0/.
:: Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>

set cache_dir=%userprofile%\.cache
set cctwinutil_cache_dir=%cache_dir%\cctwinutil

if exist %cctwinutil_cache_dir% ( rmdir /s /q %cctwinutil_cache_dir% )

if not exist %cache_dir% ( mkdir %cache_dir% )
if not exist %cctwinutil_cache_dir% ( mkdir %cctwinutil_cache_dir% )

cd %cctwinutil_cache_dir%
echo %cd%

if exist cttwinutil.zip ( del cttwinutil.zip )
if exist winutil-main ( rmdir /s /q winutil-main )

curl -L -o cttwinutil.zip https://github.com/ChrisTitusTech/winutil/archive/refs/heads/main.zip
tar xf cttwinutil.zip

cd winutil-main
pwsh.exe .\winutil.ps1

if exist %cctwinutil_cache_dir% ( rmdir /s /q %cctwinutil_cache_dir% )
