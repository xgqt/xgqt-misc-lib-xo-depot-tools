@echo off

:: This Source Code Form is subject to the terms of the Mozilla Public
:: License, v. 2.0. If a copy of the MPL was not distributed with this
:: file, You can obtain one at http://mozilla.org/MPL/2.0/.
:: Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>

pwsh.exe -c Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Bypass
