let pkgs = import <nixpkgs> { };
in pkgs.mkShell rec {
  name = "xo-depot-tools-v1";

  nativeBuildInputs = with pkgs.buildPackages; [
    bash
    curl
    gnumake
    python3

    git
    glibcLocales
  ];

  shellHook = ''
    PS1="${name}> "
  '';
}
