#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

PATH="$(pwd)/bin:${PATH}" ; export PATH

completion_backend_type=""

case "$(cat /proc/$$/comm)" in
    bash )
        completion_backend_type="bash"
        ;;
    zsh )
        completion_backend_type="zsh"
        ;;
esac

if [ -n "${completion_backend_type}" ] ; then
    commands_to_complete=(
        helm
        k3d
        kind
        kubectl
        nerdctl
    )
    command_to_complete=""

    for command_to_complete in "${commands_to_complete[@]}" ; do
        source <("${command_to_complete}" completion "${completion_backend_type}")
    done

    unset commands_to_complete
    unset command_to_complete
fi

unset completion_backend_type
