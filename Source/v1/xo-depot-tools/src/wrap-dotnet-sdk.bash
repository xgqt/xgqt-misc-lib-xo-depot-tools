#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r depot_root="$(realpath "${script_root}/../")"
declare -r bin_root="${depot_root}/bin"

PATH="${bin_root}:${PATH}" ; export PATH

if ! command -v dotnet >/dev/null 2>&1 ; then
    bash "${script_root}/download-dotnet-sdk.bash"

    if [[ -d "${depot_root}/lib/dotnet-sdk" ]] ; then
        DOTNET_ROOT="${depot_root}/lib/dotnet-sdk" ; export DOTNET_ROOT
    fi
fi

if [[ -z "${DOTNET_CLI_TELEMETRY_OPTOUT}" ]] ; then
    DOTNET_CLI_TELEMETRY_OPTOUT="1" ; export DOTNET_CLI_TELEMETRY_OPTOUT
fi

if [[ -z "${DOTNET_SKIP_FIRST_TIME_EXPERIENCE}" ]] ; then
    DOTNET_SKIP_FIRST_TIME_EXPERIENCE="1" ; export DOTNET_SKIP_FIRST_TIME_EXPERIENCE
fi

exec dotnet "${@}"
