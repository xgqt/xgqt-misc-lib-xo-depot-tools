#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r depot_root="$(realpath "${script_root}/../")"
declare -r bin_root="${depot_root}/bin"

# Put "bin_root" last as we want to use this podman version is last resort!
PATH="${PATH}:${bin_root}" ; export PATH

if ! command -v podman >/dev/null 2>&1 ; then
    bash "${script_root}/download-podman.bash"
fi

exec podman "${@}"
