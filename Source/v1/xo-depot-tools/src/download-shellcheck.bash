#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r source_root="$(realpath "${script_root}/../")"
declare -r bin_root="${source_root}/bin"
declare -r lib_root="${source_root}/lib"
declare -r tmp_root="${source_root}/.cache/tmp"

declare DTV_SHELLCHECK
: "${DTV_SHELLCHECK:=0.10.0}"

declare -r pn="shellcheck"
declare -r pv="${DTV_SHELLCHECK}"
declare -r exe_path="${bin_root}/${pn}"

if [[ -e "${exe_path}" ]] ; then
    echo " [DBG] ${pn} is already installed as ${exe_path}"

    exit 0
fi

echo " [DBG] Downloading ${pn}"
set -x

mkdir -p "${bin_root}"
mkdir -p "${tmp_root}"
mkdir -p "${lib_root}/static"

declare -r os_arch="$(uname -m)"
declare p=""

case "${os_arch}" in
    aarch64 )
        p="${pn}-v${pv}.linux.aarch64"
        ;;
    x86_64 )
        p="${pn}-v${pv}.linux.x86_64"
        ;;
    * )
        echo " [ERR] Unsupported architecture"

        exit 1
        ;;
esac

declare -r src_uri="https://github.com/koalaman/${pn}/releases/download/v${pv}/${p}.tar.xz"
declare -r s="${pn}-v${pv}"

cd "${tmp_root}"
curl --location --silent --output "${pn}.tar.xz" "${src_uri}"

tar xf "${pn}.tar.xz"
mv "./${s}/${pn}" "${lib_root}/static"

ln -f -s "../lib/static/${pn}" "${exe_path}"
rm -r "${tmp_root}"
