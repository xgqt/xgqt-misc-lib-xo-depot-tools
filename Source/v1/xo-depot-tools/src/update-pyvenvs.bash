#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r source_root="$(realpath "${script_root}/../")"
declare -r lib_root="${source_root}/lib"
declare -r pyvenvs_root="${lib_root}/python-virtual-environment"

declare pyvenv_directory=""
declare pyvenv_main_package=""

while read -r pyvenv_directory ; do
    if [[ "${pyvenv_directory}" == "${pyvenvs_root}" ]] ; then
        continue
    fi

    pyvenv_main_package="$(basename "${pyvenv_directory}")"

    echo " [DBG] Starting work in directory: ${pyvenv_directory}"
    echo " [DBG] Main Python package is: ${pyvenv_main_package}"

    (
        source "${pyvenv_directory}/bin/activate"

        python -m pip install --upgrade "${pyvenv_main_package}" \
            || echo " [ERR] Could not update package: ${pyvenv_main_package}"
    )
done < <(find "${pyvenvs_root}" -maxdepth 1 -type d -not -empty | sort)
