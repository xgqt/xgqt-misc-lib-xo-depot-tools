#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r source_root="$(realpath "${script_root}/../")"
declare -r bin_root="${source_root}/bin"
declare -r lib_root="${source_root}/lib"
declare -r tmp_root="${source_root}/.cache/tmp"

declare DTV_KUBECTL
: "${DTV_KUBECTL:=1.31.1}"

declare -r pn="kubectl"
declare -r pv="${DTV_KUBECTL}"
declare -r exe_path="${bin_root}/${pn}"

if [[ -e "${exe_path}" ]] ; then
    echo " [DBG] ${pn} is already installed as ${exe_path}"

    exit 0
fi

echo " [DBG] Downloading ${pn}"
set -x

mkdir -p "${bin_root}"
mkdir -p "${tmp_root}"
mkdir -p "${lib_root}/static"

declare -r os_arch="$(uname -m)"
declare src_uri=""

case "${os_arch}" in
    x86_64 )
        src_uri="https://dl.k8s.io/release/v${pv}/bin/linux/amd64/${pn}"
        ;;
    * )
        echo " [ERR] Unsupported architecture"

        exit 1
        ;;
esac

cd "${lib_root}/static"
curl --location --silent --output "${pn}" "${src_uri}"

chmod a+x "${pn}"
ln -f -s "../lib/static/${pn}" "${exe_path}"
