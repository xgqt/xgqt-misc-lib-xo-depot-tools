#!/usr/bin/env bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

declare -r script_path="${0}"
declare -r script_root="$(dirname "$(realpath "${script_path}")")"

declare -r source_root="$(realpath "${script_root}/../")"
declare -r bin_root="${source_root}/bin"
declare -r lib_root="${source_root}/lib"
declare -r tmp_root="${source_root}/.cache/tmp"

declare DTV_KIND
: "${DTV_KIND:=0.22.0}"

declare -r pn="kind"
declare -r pv="${DTV_KIND}"
declare -r exe_path="${bin_root}/${pn}"

if [[ -e "${exe_path}" ]] ; then
    echo " [DBG] ${pn} is already installed as ${exe_path}"

    exit 0
fi

echo " [DBG] Downloading ${pn}"
set -x

mkdir -p "${bin_root}"
mkdir -p "${tmp_root}"
mkdir -p "${lib_root}/static"

declare -r os_arch="$(uname -m)"
declare p=""

case "${os_arch}" in
    x86_64 )
        p="${pn}-linux-amd64"
        ;;
    * )
        echo " [ERR] Unsupported architecture"

        exit 1
        ;;
esac

declare -r src_uri="https://github.com/kubernetes-sigs/${pn}/releases/download/v${pv}/${p}"

cd "${lib_root}/static"
curl --location --silent --output "${pn}" "${src_uri}"

chmod a+x "${pn}"
ln -f -s "../lib/static/${pn}" "${exe_path}"
