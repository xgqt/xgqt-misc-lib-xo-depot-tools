# XO Depot Tools

Xgqt.Org depot tools

## About

A collection of tools for software development inspired by the Google
ChromeOS's Depot Tools.

### What is missing?

* `bash` (*wanted: static / libc-independent*)
* `bubblewrap` (*wanted: static* **tbd**)
* `busybox` (*wanted: static* **tbd**)
* `make` (*wanted: static* **tbd**)
* `openjdk` (*wanted: static / libc-independent*)
* `qemu` (*wanted: static / libc-independent*)
* `rust` (*wanted: libc-independent (static build is impossible)*)
* real `podman` (*included is just a client*)

## Installation

### Required system tools

* The `bash` shell is a hard dependency.
* The `curl` utility is a hard dependency for `download-*.bash` scripts.
* To install any python packages you will need system install of Python
  that has the `venv` module included.
* `git` for development, bootstrapping and adding as a git submodule.

Additionally the following basic UNIX utilities are required:

* `basename`
* `chmod`
* `dirname`
* `find`
* `ln`
* `realpath`
* `rm`
* `sort`
* `tar` (with GZip and XZ/LZMA support)
* `uname`

Most of the above utilities should be provided either by your operating
system's UNIX implementation of coreutils-like package and/or `busybox`.

### Under a specified user account

1. In the `Source/v1/xo-depot-tools` directory run `make`.

   For example by executing the following code snippet:

   ```shell
   make -C Source/v1/xo-depot-tools
   ```

2. Add `Source/v1/xo-depot-tools/bin` (as a absolute directory path)
   to your user's `PATH` variable.

   For example by executing the following code snippet:

   ```shell
   PATH="${PATH}:$(pwd)/Source/v1/xo-depot-tools/bin"
   echo "PATH=\"${PATH}\" ; export PATH" >> ~/.profile
   ```

### Bootstrap as a submodule with a script

To quickly install XO-Depot-Tools as a git submodule inside a local git
repository and check it out to the latest tagged version you can execute
the following Bash code snippet:

```shell
bash <(curl -L "https://gitlab.com/xgqt/xgqt-misc-lib-xo-depot-tools/-/raw/master/Source/v1/admin/bootstrap_submodule.bash")
```

## License

### Code

Code in this project is licensed under the MPL, version 2.0.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
