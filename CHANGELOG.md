## 2.0.0 (2024-10-27)

### Feat

- start v2
- **xo-depot-tools/src**: add wraps for bsd* tools from libarchive
- **xo-depot-tools/src**: add download-libarchive.bash
- **xo-depot-static/src**: add build-libarchive.bash
- **xo-depot-static/Earthfile**: setup for libarchive
- **src/download-bazel**: bump DTV_BAZELISK - 1.19.0 -> 1.20.0
- **xo-depot-tools**: add kubernetes-validate support
- **xo-depot-tools**: add nixfmt support
- **xo-depot-windows-tweaks/src**: add pwsh_set_policy_bypass.cmd
- **xo-depot-windows-tweaks/src**: add run_cttwinutil.cmd
- **admin**: add show_tools.bash
- **xo-depot-windows-tweaks/src**: add install_winget_packages_machine.cmd
- **xo-depot-tools/src**: add wrap-rsync.bash
- **xo-depot-tools/src**: add wrap-ninja.bash
- **xo-depot-tools/src**: add download-ninja.bash
- **xo-depot-tools/src**: add download-rsync.bash
- **xo-depot-static/src**: add build-rsync.bash
- **xo-depot-static/src/build-ninja.bash**: create dist zip
- **xo-depot-tools/src**: add wrap-meson.bash
- **xo-depot-tools/src**: add download-meson.bash
- **xo-depot-tools/src**: allow to control tool versions
- **xo-depot-static/src/build-ninja.bash**: allow to control tool version
- **xo-depot-static**: add Makefile
- **xo-depot-static**: add build-all.bash
- **xo-depot-static/src**: add build-ninja.bash
- **xo-depot-tools/src**: add download-k3s.bash and wrap-k3s.bash
- **xo-depot-tools/download-all.bash**: skip azure-cli by default
- **xo-depot-tools/src**: add wrap-git-lfs.bash
- **xo-depot-tools/src**: add download-git-lfs.bash

### Fix

- **sh/wrap-generic**: six depot_root
- **src/build-libarchive**: fix static build
- **xo-depot-tools/src**: use python3 exe
- **src/wrap-bazel**: check and exec bazelisk
- **xo-depot-tools/src/download-pandoc.bash**: set fallback for DTV_PANDOC
- **earthly**: remove call to download-jq.bash as it duplicates task in ci_test.sh
- **xo-depot-tools/src/wrap-earthly.bash**: fix setting of earthly-specific variables
- **xo-depot-static/src**: add os type (linux) to archive/exe names
- **xo-depot-tools/src**: correct ":-" to ":=" in order to set the variable
- **xo-depot-static/build-all.bash**: make executable
- **editorconfig**: use crlf in bat and cmd scripts

### Refactor

- **src/build-libarchive**: move instead of ln and do not duplicate
- **Copyright/LICENSE_HEADER.txt**: update license header text
- **admin/bootstrap_submodule.bash**: update copyright info
- **xo-depot-windows-tweaks/src**: add copyright info
- **xo-depot-tools/src**: use read-only declare
- **xo-depot-static/src**: add licenses of statically linked libraries
- **xo-depot-static**: add .earthlyignore
- **xo-depot-static/src/build-ninja.bash**: drop --verbose
- **xo-depot-static/src/build-ninja.bash**: ninja -> pn
- **xo-depot-static**: ignore the dist directory
- **xo-depot-static**: add .gitignore

## 1.2.0 (2024-05-02)

### Feat

- **admin/bootstrap_submodule.bash**: create submodule_directory parents
- **admin**: add bootstrap_submodule.bash
- **xo-depot-tools/src**: add wrap-go.bash
- **xo-depot-tools/src**: add download-go.bash
- **xo-depot-tools/src**: add wrap-ncdu.bash
- **xo-depot-tools/src**: add download-ncdu.bash
- **xo-depot-tools/src**: add wrap-pandoc.bash
- **xo-depot-tools/src**: add download-pandoc.bash
- **xo-depot-tools/src**: add wrap-shellcheck.bash
- **xo-depot-tools/src**: add download-shellcheck.bash
- **xo-depot-tools/src**: add update-pyvenvs.bash
- **xo-depot-tools/src**: add wrap-yapf.bash
- **xo-depot-tools/src**: add download-yapf.bash
- **xo-depot-tools/src**: add wrap-black.bash
- **xo-depot-tools/src**: add download-black.bash
- **xo-depot-tools/src/wrap-earthly.bash**: disable analytics

### Fix

- **admin/bootstrap_submodule.bash**: pre-set env-vars
- **xo-depot-tools/src/download-commitizen.bash**: set pn to commitizen, not cz
- **xo-depot-tools/src/download-black.bash**: link blackd; install aiohttp

## 1.1.0 (2024-04-30)

### Feat

- **xo-depot-tools/src**: add missing wrappers
- **xo-depot-tools/src**: add wrap-mkdocs.bash
- **xo-depot-tools/src/wrap-dotnet-sdk.bash**: export helpful variables
- **xo-depot-tools/src**: add wrap-bazel.bash
- **xo-depot-tools/src**: add wrap-dotnet-sdk.bash

### Fix

- **xo-depot-tools/src**: use realpath

## 1.0.0 (2024-04-30)

### Feat

- **xo-depot-tools/src**: add download-wgetpaste.bash
- **xo-depot-tools/src**: add download-gh.bash
- **xo-depot-tools/src**: add download-linode-cli.bash
- **xo-depot-tools/src**: add download-dtrx.bash
- **xo-depot-tools/src**: add download-tea.bash
- **xo-depot-tools/src**: add download-rclone.bash
- **xo-depot-tools/src**: add download-yamllint.bash
- **xo-depot-tools/activate-environment.sh**: add helm
- **xo-depot-tools/src**: add download-helm.bash
- **xo-depot-tools/src**: add download-nerdctl.bash
- **xo-depot-tools/activate-environment.sh**: source autocompletions
- **xo-depot-tools/src**: add download-kubectl.bash
- **xo-depot-tools/src**: add download-commitizen.bash
- **xo-depot-tools/src**: add download-k3d.bash
- **xo-depot-tools/src**: add download-kind.bash
- **xo-depot-tools/src**: add download-azure-cli.bash
- **xo-depot-tools**: add activate-environment.sh
- **xo-depot-tools/src**: add download-tbump.bash
- **xo-depot-tools/src**: add download-pre-commit.bash
- **xo-depot-tools/src**: add download-bazel.bash
- **xo-depot-tools**: add download-zig.bash
- **xo-depot-tools**: add download-yq.bash
- **xo-depot-tools**: add download-jq.bash
- **xo-depot-tools**: add download-k9s.bash

### Fix

- **xo-depot-tools/src**: remove loose download-nerdctl.bash
- **xo-depot-tools/activate-environment.sh**: use array
- **xo-depot-tools/src/download-zig.bash**: use p instead of s

### Refactor

- **xo-depot-tools/src**: sue proper case for arches
- **xo-depot-tools/src/download-dotnet-sdk.bash**: dotnet-sdk-root -> dotnet-sdk
- **xo-depot-tools/src/download-mkdocs.bash**: use vars in pkg array
- **development**: add boilerplate config files to repo root
- **xo-depot-tools/src/download-podman.bash**: cleanup
- **xo-depot-tools/src/download-earthly.bash**: move to lib/static; link to bin
- **xo-depot-tools/src/download-podman.bash**: move to lib/static; link to bin
- **xo-depot-tools/src/download-k9s.bash**: move to lib/static; link to bin
- **xo-depot-tools**: use relative symlink to dotnet
- **xo-depot-tools**: use ${pn} in download-dotnet-sdk.bash
- **xo-depot-tools**: use ${pn} in download-earthly.bash
- **xo-depot-tools**: use ${pn} in download-podman.bash
- **xo-depot-tools**: use relative symlink in download-mkdocs.bash
- **xo-depot-tools**: add .gitignore
